import game

"""
Used Later
"""
players = {}

#create the game state based on a human chosen number of players
print "how many players?"
num_players = int(raw_input())

print "would you like to test your AI? y/n"
test = str(raw_input())

if test == 'n':
	print "What would you like your name to be? string"
	team_name = str(raw_input())
else:
	team_name = 'not relevant'


#initialize a single game
new_game = game.Game(num_players,test, team_name)


#take turns until the last card is drawn
while len(new_game.deck) > 0 :
	new_game.take_turn()

#print out the final game state regardless of whose turn it ends on
new_game.game_state(force=True)

#figuring out who wins in a really clunky way
scores = [new_game.players[i].score for i in xrange(len(new_game.players))]
names = [new_game.players[i].name for i in xrange(len(new_game.players))]

#create a dictionary [score]: player to determine each game's winner 
d = dict((key, value) for (key, value) in zip(scores,names))

#creating a reverse dictionary to show an AI performamce
for j in xrange(len(names	)): 
	players[names[j]] = d[scores[j]]

#arrange scores lowest to highest
scores.sort()

print 'game over'
print 'congrats ' + d[scores[0]] + " with the lowest score of " + str(scores[0])

if test == 'y':
	print 'your AI won' + str(players['My Robot']) + " games."