
class Player():
	"""
	Generic Player Class

	Cards: List of integers
	Tokens: Integer
	Score: Integer
	type: string, used to determine if it's human player or an AI
	name: string, provides clarity for human players.
	"""

	def __init__(self,n,name,kind):
		self.cards = []
		self.tokens = n
		self.score = 0
		self.type = kind
		self.name = str(name)

	def scoring(self):
		self.cards = sorted(self.cards)
		self.score = 0

		#check that the cards aren't empty
		if len(self.cards) > 0:
			self.score += self.cards[0]
		for i in xrange(len(self.cards)):
			if i > 0:
				if self.cards[i] != self.cards[i-1] + 1:
					self.score += self.cards[i]
		self.score -= self.tokens


class Human(Player):
	
	def player_choice(self,tokens,card,game_state):
		print self.name
		print 'would you like to take the ' + str(card) + ' there are ' + str(tokens) + " tokens on it"
		choice = str(raw_input())
		
		#ensures valid input
		while (choice != 'y') and (choice != 'n'):
			print 'invalid input try again'
			choice = str(raw_input())
		

		#check validity of the move based on tokens
		if choice == 'y':
			pass
		elif choice == 'n':
			if self.tokens == 0:
				print 'you have no more tokens you must take the card'
				choice = 'y'
			else:
				self.tokens -= 1

		return choice


class Player_AI(Player):

	
	def player_choice(self,tokens,card,game_state):
		#check validity of the move based on tokens
		if self.tokens > 0:

			"""
			Create an algorithm that decides to take the card or not take the card

			INPUTS:
				tokens, integer 	: 	The number of tokens on the card
				card, integer    	: 	The value of the card
				game_state, string 	: 	Which cards belong to which players and how many tokens they have
				
			game_state starts with the number of players on the first line and lists the players cards 

			"""
			pass
			
		else:
			choice = 'f'

			
		return choice


class Test_AI(Player):

	
	def player_choice(self,tokens,card):
		#check validity of the move based on tokens
		if self.tokens > 0:

			"""
			Create an algorithm that decides to take the card or not take the card

			INPUTS:
				tokens, integer 	: 	The number of tokens on the card
				card, integer    	: 	The value of the card
				game_state, string 	: 	Which cards belong to which players and how many tokens they have
				
			game_state starts with the number of players on the first line and lists the players cards 

			"""

			if (card-1 in self.cards) or (card+1 in self.cards):
				choice = 'y'
				print 'Found one'
			
			#Substantial amount of tokens on the card take it
			elif tokens >= card-6:
				print "I guess"
				choice = 'y'
			
			#future decision making power 
			elif tokens >= 7:
				print "worth it"
				choice = 'y'

			#failed all checks and there's tokens remaining
			else:
				choice = 'n'
				self.tokens -= 1
			
		else:
			choice = 'f'

			
		return choice

