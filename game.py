import random
import player


class Game():

	def __init__(self,num_players,test,team_name):
		self.num_players = num_players
		#create a deck with cards and determine randomly which cards to remove
		if num_players >= 6:
			self.deck = range(1,35+num_players*4)
			mylist = random.sample(range(1,34+num_players*4),num_players)
			tokens = 8
		else:
			self.deck = range(1,35)
			mylist = random.sample(range(1,35),5)
			tokens = 45/num_players

		#remove values from the deck
		counter = 1
		for j in xrange(len(mylist)):
			#print mylist[j]-counter,len(self.deck)
			self.deck.pop(mylist[j]-counter)
			counter += 1 
			


		#fill the roster with players
		self.players = []
		if test == 'y':
			for i in xrange(num_players-1):
				self.players.append(player.Test_AI(tokens,'player ' + str(i+1),'AI'))
				#self.players.append(player.Human(tokens, player_names[i])
			
			#add in the human player/s
			self.players.append(player.Player_AI(tokens,'My Robot','Human'))


		else:	
			for i in xrange(num_players-1):
				self.players.append(player.Test_AI(tokens,'player ' + str(i+1),'AI'))
				#self.players.append(player.Human(tokens, player_names[i])
			
			#add in the human player/s
			self.players.append(player.Human(tokens,team_name,'Human'))


		#turns
		self.current_player = -1
		#number of tokens on the card
		self.card_tokens = 0

	def draw_card(self):
		
		card_loc = random.randint(0,len(self.deck)-1)
		card = self.deck[card_loc]
		self.deck.pop(card_loc)
		return card

	def take_turn(self,):
		#draw the card for the turn
		card = self.draw_card()
		
		#move to the next player at the start of the turn
		self.current_player += 1

		#determine which player takes the card
		self.player_decision(card)

		#add the card and token's to the person's pool
		self.players[self.current_player%self.num_players].cards.append(card)
		self.players[self.current_player%self.num_players].tokens += self.card_tokens		
		self.players[self.current_player%self.num_players].scoring()		
		
		#reset the current tokens on the card
		self.card_tokens = 0


	def player_decision(self,card):
		"""
		Determine which player takes the card by recursively asking for a decision until a player chooses yes
		"""
		if self.players[self.current_player%self.num_players].type is 'Human':
			choice = self.players[self.current_player%self.num_players].player_choice(self.card_tokens,card,self.game_state(force=False))
		else:
			choice = self.players[self.current_player%self.num_players].player_choice(self.card_tokens,card)

		#recursively move through the players until a decision is made
		if choice == 'n':
			self.current_player += 1
			self.card_tokens += 1
			self.player_decision(card)
		elif choice == 'f':
			self.current_player += 0

	
	def game_state(self, force):
		"""
		create a string that represents players's tokens, which cards they have, and
		what their current score is.
		
		Game State is printed out on human player's turns
		
		returns a string formatted 
		"""
		state = ''
		state += str(len(self.players))

		for i in xrange(len(self.players)):
			#update scores
			self.players[i].scoring()
			just_cards = str(self.players[i].cards)[1:-1]
			just_cards = just_cards.replace(",",'')
			state += '\n' + just_cards
			state += '\n' + str(self.players[i].tokens)
			state += '\n' + str(self.players[i].score)

			#print game state in a readable manner for humans
			if (self.players[self.current_player%self.num_players].type == 'Human') or (force==True):
				if (force == True) and (i == 0):
					print 'Final Game State'
				print self.players[i].name
				print "cards:  ", self.players[i].cards
				print "tokens: ", self.players[i].tokens
				print "score:  ", self.players[i].score
		
		return state